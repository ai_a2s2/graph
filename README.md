# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

5. Monochromatic Triangle (EA and PSO)
In an undirected graph G (V , E) with the set of vertices V and the set of edges E
find a partitioning of two disjoint subsets E1 and E2 so that each of the two sub-graphs
G (V , E ) and does not contain a triangle - any three different nodes , , 1 1 G (V , E ) 2 2 u v w
from V are not connected with edges from the same sub-graph ((u, v) , (u,w) and (v,w)
are not in E ).
___________________________________________________________________________
For the initial graph G with V = {1, 2, 3, 4, 5} and E = {(1, 2), (1, 3), (1, 4), (1, 5),(2, 3),(3, 4),
(3, 5), (4, 5)} a solution can be :
E {(1, ), (1, 4), (3, ), (4, )} and . 1 = 2 5 5 E {(1, ), (1, ), (2.3), (3, )} 2 = 3 5 4
___________________________________________________________________________
Attention: E U E E and , 1 2 = E1∩ E2 = Φ
but E1 = {(1, 2), (1, 4), (3, 4), (3, 5), (4, 5)} and E2 = {(1, 3), (1, 5),(2.3)} is not a solution because
you have a triangle in E between the nodes 3, 4 and 5. 